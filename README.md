# Project 2: Android Programming Coursework #

### This coursework develops the Quiz application described in lectures. ###

Open up the Eclipse IDE that you have been using for Android work.  Select File-> Import.  From the window that pops up, double click on General, and then on Existing Projects into Workspace. A new window pops up, choose Select archive file: and  enter the location of Quiz1.zip, and then press the Finish button

The project Quiz will appear in your workspace.  Double clicking on the source file will open it in an editor.  Once it is open, you can can select Run  The application starting on the emulated phone will show two buttons, and when either is pressed an informative message pops up.

Two other projects should be imported into the workspace from maths.zip and multichoice.zip

maths.zip is an Android activity that displays randomly generated arithmetic questions and checks the answer that the user provides.  multichoice.zip is an Android activity that displays multiple choice questions (on history).

The coursework task is to combine these activities into a single application, so that the pressing of the button in the Quiz activity starts the maths activity or the mutlichoice activity.  In addition, it should accumulate the scores that are achieved in each test.

You should make the application **display your own name, so that it is Peter's Quiz** (if you name is Peter!).  **Extra credit can be gained by introducing a third set of questions, say on Geography.**
