package za.co.cti.quizapp.games.maths;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import za.co.cti.quizapp.R;
import za.co.cti.quizapp.Select_Game_Activity;
import za.co.cti.quizapp.user.Person;

import static za.co.cti.quizapp.R.id.action_settings;
import static za.co.cti.quizapp.R.id.maths_answer_button;
import static za.co.cti.quizapp.R.id.maths_answer_text;
import static za.co.cti.quizapp.R.id.maths_question_text;
import static za.co.cti.quizapp.R.id.maths_summary;
import static za.co.cti.quizapp.R.layout.activity_maths_questions;

public class Maths_Questions_Activity extends ActionBarActivity implements OnClickListener {

    private TextView question;
	private TextView summary;
	private EditText answer;
	private Button answerButton;
	
	private int answerValue;
	private int operation;
	private String questionText;
	private int operand1Value;
	private int operand2Value;
	
	private int noQuestions = 10;
	private int noAnswered;
	private int noCorrect;
	private int _null;

   /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_maths_questions);

        this.getCurrentScore();

        Log.i("ARITH", "Started");

        question = (TextView) findViewById(maths_question_text);
        answer = (EditText)findViewById(maths_answer_text);
        summary = (TextView) findViewById(maths_summary);
        answerButton = (Button)findViewById(maths_answer_button);
        answerButton.setOnClickListener(this);
        noAnswered = 0;
        noCorrect = 0;

        generateSummary();
        generateQuestion();
   }

    private void getCurrentScore() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0
        TextView mathCurrentScore = (TextView) findViewById(R.id.current_highScore);

        // getting String
        int mathScore = pref.getInt("mathScore", _null);

        if (mathScore >= 1) {
            // Display name on textView
            mathCurrentScore.setText(String.format("Your current score is %s.", mathScore));
        }
    }

    private void generateSummary() {
    	summary.setText( noCorrect + " answers out of " + noAnswered 
    			+ " attempted of " +noQuestions + " questions");
    }
    
	private void generateQuestion() {
        operation = (int)(Math.random()*3.0);
        operand1Value = (int) (Math.random()*19.0) + 1;
        operand2Value = (int) (Math.random()*19.0) + 1;
        switch (operation) {
        case 0:
        	questionText = operand1Value + " + " + operand2Value;
        	answerValue = operand1Value + operand2Value;
        	break;
        case 1:
        	questionText = operand1Value + " x " + operand2Value;
        	answerValue = operand1Value * operand2Value;
        	break;
        case 2:
        	if ( operand1Value < operand2Value ) {
        		answerValue = operand2Value;
        		operand2Value = operand1Value;
        		operand1Value = answerValue;
        	}
        	questionText = operand1Value + " - " + operand2Value;
        	answerValue = operand1Value - operand2Value;
        	break;
       	default:
       		break;
        }
        question.setText(questionText);
	}

    public void onClick(View arg0) {

        if (arg0 == answerButton) {
            int response = 0;
            noAnswered++;

            try {
                response = Integer.decode(answer.getText().toString());
                if (response == answerValue) {
                    noCorrect++;
                }
            } catch (NumberFormatException nfe) {
                // can't be a correct answer, so ignore it!
            }

            answer.setText("");
            generateSummary();

            if (noAnswered < noQuestions)
                generateQuestion();
            else {
                finish();
                Intent intentBundle = getScore();
                startActivity(intentBundle);
            }

        }
    }

    private Intent getScore() {
        // Create an intent pass class name
        Intent intentBundle = new Intent(Maths_Questions_Activity.this, Select_Game_Activity.class);

        Bundle bundle = new Bundle();

        bundle.putInt("mathScore", noCorrect);

        intentBundle.putExtras(bundle);
        return intentBundle;
    }
}