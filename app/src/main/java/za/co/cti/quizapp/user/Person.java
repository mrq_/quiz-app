package za.co.cti.quizapp.user;

import java.io.Serializable;

/**
 * Created by mohau on 15/03/10.
 */
public class Person implements Serializable{

    private static final long serialVersionUID = 1L;

    private String name;

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String s) {
        this.name = s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!name.equals(person.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        return result;
    }
}
