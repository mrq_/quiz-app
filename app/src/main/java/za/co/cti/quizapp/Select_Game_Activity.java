package za.co.cti.quizapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import za.co.cti.quizapp.games.geography.Geography_Questions_Activity;
import za.co.cti.quizapp.games.history.History_Questions_Activity;
import za.co.cti.quizapp.games.maths.Maths_Questions_Activity;
import za.co.cti.quizapp.user.Person;

public class Select_Game_Activity extends ActionBarActivity implements View.OnClickListener {

    private String TAG = "intent_data";

    private Button historyButton;
    private Button mathsButton;
    private Button geography_button;
    private int MathScore, HistoryScore, GeographyScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_game);

        // Get passed intent
        Intent intent = getIntent();

        // Get the bundle
        Bundle extraBundle = intent.getExtras();

        // Get reference to person textView
        TextView personName = (TextView) findViewById(R.id.personName);
        TextView personScore = (TextView) findViewById(R.id.overall_summary);
        TextView totalScore = (TextView) findViewById(R.id.totalScore);

        personName.setTextSize(25);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0
        SharedPreferences.Editor editor = pref.edit();

        // getting String
        String _name = pref.getString("person", null);

        // Display name on textView
        personName.setText(String.format("Welcome %s!", _name));

        boolean hasMathScore = extraBundle.containsKey("mathScore");
        boolean hasHistoryScore = extraBundle.containsKey("historyScore");
        boolean hasGeographyScore = extraBundle.containsKey("geographyScore");

        this.getBundleExtra(extraBundle, personScore, editor, hasMathScore, hasHistoryScore, hasGeographyScore);

        if (!extraBundle.isEmpty()) {
            if (hasMathScore && hasHistoryScore && hasGeographyScore) {
                MathScore = extraBundle.getInt("mathScore");
                HistoryScore = extraBundle.getInt("historyScore");
                GeographyScore = extraBundle.getInt("geographyScore");

                int TOTAL = MathScore + HistoryScore + GeographyScore;
                totalScore.setText(String.format("Your current high score is %s!", TOTAL));

            } else if (hasMathScore && hasHistoryScore) {
                MathScore = extraBundle.getInt("mathScore");
                HistoryScore = extraBundle.getInt("historyScore");
                int TOTAL = MathScore + HistoryScore;
                totalScore.setText(String.format("Your current high score is %s!", TOTAL));

            } else if (hasMathScore && hasGeographyScore) {
                MathScore = extraBundle.getInt("mathScore");
                GeographyScore = extraBundle.getInt("geographyScore");

                int TOTAL = MathScore + GeographyScore;
                totalScore.setText(String.format("Your current high score is %s!", TOTAL));

            } else if (hasHistoryScore && hasGeographyScore) {
                HistoryScore = extraBundle.getInt("historyScore");
                GeographyScore = extraBundle.getInt("geographyScore");

                int TOTAL = HistoryScore + GeographyScore;
                totalScore.setText(String.format("Your current high score is %s!", TOTAL));

            }
        }

        buttonSettings();
    }

    private void getBundleExtra(Bundle extraBundle, TextView personScore, SharedPreferences.Editor editor, boolean hasMathScore, boolean hasHistoryScore, boolean hasGeographyScore) {
        // Get person object from intent

        if (!extraBundle.isEmpty()) {

            if (hasMathScore) {
                MathScore = extraBundle.getInt("mathScore");
                personScore.setText("You scored " + String.valueOf(MathScore) + " out of 10, on your Maths Quiz.");
                editor.putInt("mathScore", MathScore);

            } else if (hasHistoryScore) {
                HistoryScore = extraBundle.getInt("historyScore");
                editor.putInt("historyScore", HistoryScore);
                personScore.setText("You scored " + String.valueOf(HistoryScore) + " out of 5, on your History Quiz.");

            } else if (hasGeographyScore) {
                GeographyScore = extraBundle.getInt("geographyScore");
                editor.putInt("geographyScore", GeographyScore);
                personScore.setText("You scored " + String.valueOf(GeographyScore) + " out of 6, on your Geography Quiz.");

            } else {
                // No bundles
                editor.remove("mathScore");
                editor.remove("historyScore");
                editor.remove("geographyScore");
            }
            // commit changes
            editor.commit();
        }
    }

    private void buttonSettings() {

        mathsButton = (Button) findViewById(R.id.maths_button);
        mathsButton.setOnClickListener(this);

        historyButton = (Button) findViewById(R.id.history_button);
        historyButton.setOnClickListener(this);

        geography_button = (Button) findViewById(R.id.geography_button);
        geography_button.setOnClickListener(this);
    }

    public void onClick(View arg0) {
        if (arg0 == mathsButton) {
            Log.i("QUIZ", "Maths selected");

            Intent intent = new Intent(this, Maths_Questions_Activity.class);
            startActivity(intent);

            // Toast.makeText(getApplicationContext(), "Maths button pressed", Toast.LENGTH_LONG).show();
        } else if (arg0 == historyButton) {
            Log.i("QUIZ", "History selected");

            Intent intent = new Intent(this, History_Questions_Activity.class);
            startActivity(intent);

            // Toast.makeText(getApplicationContext(), "History button pressed", Toast.LENGTH_LONG).show();
        } else if (arg0 == geography_button) {
            Log.i("QUIZ", "Geography selected");

            Intent intent = new Intent(this, Geography_Questions_Activity.class);
            startActivity(intent);

            // Toast.makeText(getApplicationContext(), "Geography button pressed", Toast.LENGTH_LONG).show();
        }

    }

}