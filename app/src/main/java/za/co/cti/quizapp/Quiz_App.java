package za.co.cti.quizapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import za.co.cti.quizapp.user.Person;

public class Quiz_App extends ActionBarActivity implements View.OnClickListener {

    Button btnPlay;
    EditText personName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_app);

        btnPlay = (Button) findViewById(R.id.btnPlay);
        personName = (EditText) findViewById(R.id.personName);

        btnPlay.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        // Create an intent pass class name
        Intent intent = new Intent("za.co.cti.quizapp.Select_Game_Activity");

        // Create person object
        Person person = new Person();
        person.setName(personName.getText().toString());

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0
        SharedPreferences.Editor editor = pref.edit();

        // Storing person
        editor.putString("person", person.toString());

        // commit changes
        editor.commit();

        // Put person in intent data
        intent.putExtra("person", person);

        // Start the activity
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz_app, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

}
