package za.co.cti.quizapp.games.geography;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Vector;

import za.co.cti.quizapp.Select_Game_Activity;
import za.co.cti.quizapp.games.multi_choice_questions.MultiChoiceQuestion;

import static za.co.cti.quizapp.R.id;
import static za.co.cti.quizapp.R.id.multichoice_summary;
import static za.co.cti.quizapp.R.id.question_text;
import static za.co.cti.quizapp.R.id.submit;
import static za.co.cti.quizapp.R.layout;

public class Geography_Questions_Activity extends ActionBarActivity implements OnClickListener {

    private TextView question;
    private TextView summary;
    private Vector<MultiChoiceQuestion> questions;
    private RadioGroup opts;
    private MultiChoiceQuestion currentQuestion;
    private int noCorrect;
    private int noAnswered;
    private int noQuestions = 6;
    private int _null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_multichoice_questions);

        this.getCurrentScore();

        Log.i("MCQ", "MultiChoice starts");

        opts = (RadioGroup) findViewById(id.opts);
        Button sub = (Button) findViewById(submit);
        question = (TextView) findViewById(question_text);
        summary = (TextView) findViewById(multichoice_summary);
        noCorrect = 0;
        noAnswered = 0;

        sub.setOnClickListener(this);
        initialiseQuestions();

        generateSummary();

        generateQuestion(0);
    }

    private void getCurrentScore() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0
        TextView historyCurrentScore = (TextView) findViewById(id.current_highScore);

        // getting String
        int geographyScore = pref.getInt("geographyScore", _null);

        if (geographyScore >= 1) {
            // Display name on textView
            historyCurrentScore.setText(String.format("Your current score is %s.", geographyScore));
        }
    }

    private void generateQuestion(int noQ) {

        opts.removeAllViews();
        currentQuestion = questions.get(noQ);
        question.setText(currentQuestion.getQuestion());

        int i = 1;
        for (String ans : currentQuestion.getAnswers()) {
            RadioButton rb = new RadioButton(this);
            rb.setText(ans);
            rb.setId(i++);
            opts.addView(rb);
        }
    }

    private void initialiseQuestions() {

        questions = new Vector<MultiChoiceQuestion>();
        MultiChoiceQuestion q1 = new MultiChoiceQuestion("In which country was the angel of the north erected in 1998?");
        questions.add(q1);
        q1.addAnswer("England", true);
        q1.addAnswer("Russia", false);
        q1.addAnswer("Zimbabwe", false);
        q1.addAnswer("Brazil", false);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Which home of champagne in France was also where the German high command surrendered in WW2?");
        q1.addAnswer("Aix-en-Provence", false);
        q1.addAnswer("Rennes", false);
        q1.addAnswer("Marseille", false);
        q1.addAnswer("Reims", true);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("In which European city is the Atomium?");
        q1.addAnswer("Azerbaijan", false);
        q1.addAnswer("Brussels", true);
        q1.addAnswer("Bosnia and Herzegovina", false);
        q1.addAnswer("Malta", false);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Which headland, south of Cape Town is a famous landmark for maritime navigators?");
        q1.addAnswer("Vredefort", false);
        q1.addAnswer("Witwatersrand", false);
        q1.addAnswer("Cape of Good Hope", true);
        q1.addAnswer("Vereeniging", false);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Which European town gave its name to a treaty that symbolises closer economic links between European countries?");
        q1.addAnswer("Maastricht", true);
        q1.addAnswer("Akershus", false);
        q1.addAnswer("Ardstraw", false);
        q1.addAnswer("Blackwatertown", false);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Which country lies to the north of Austria and the south of Poland?");
        q1.addAnswer("Switzerland", false);
        q1.addAnswer("France", false);
        q1.addAnswer("Czech Republic", true);
        q1.addAnswer("Belgium", false);
    }

    private void generateSummary() {
        summary.setText(noCorrect + " answers out of " + noAnswered
                + " attempted of " + noQuestions + " questions");
    }

    public void onClick(View arg0) {
        noAnswered++;

        int id;
        id = opts.getCheckedRadioButtonId();

        if (currentQuestion.isCorrect(id))
            noCorrect++;

        generateSummary();

        if (noAnswered == noQuestions) {
            finish();
            Intent intentBundle = getScore();
            startActivity(intentBundle);
        }
        else
            generateQuestion(noAnswered);
    }

    private Intent getScore() {
        // Create an intent pass class name
        Intent intentBundle = new Intent(Geography_Questions_Activity.this, Select_Game_Activity.class);

        Bundle bundle = new Bundle();

        bundle.putInt("geographyScore", noCorrect);

        intentBundle.putExtras(bundle);
        return intentBundle;
    }
}