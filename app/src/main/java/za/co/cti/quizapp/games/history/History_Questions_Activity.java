package za.co.cti.quizapp.games.history;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Vector;

import za.co.cti.quizapp.R;
import za.co.cti.quizapp.Select_Game_Activity;
import za.co.cti.quizapp.games.multi_choice_questions.MultiChoiceQuestion;

import static za.co.cti.quizapp.R.id;
import static za.co.cti.quizapp.R.id.multichoice_summary;
import static za.co.cti.quizapp.R.id.question_text;
import static za.co.cti.quizapp.R.id.submit;
import static za.co.cti.quizapp.R.layout;

public class History_Questions_Activity extends ActionBarActivity implements OnClickListener {

    private TextView question;
    private TextView summary;
    private Vector<MultiChoiceQuestion> questions;
    private RadioGroup opts;
    private MultiChoiceQuestion currentQuestion;
    private int noCorrect;
    private int noAnswered;
    private int noQuestions = 5;
    private int _null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_multichoice_questions);

        this.getCurrentScore();

        Log.i("MCQ", "MultiChoice starts");

        opts = (RadioGroup) findViewById(id.opts);
        Button sub = (Button) findViewById(submit);
        question = (TextView) findViewById(question_text);
        summary = (TextView) findViewById(multichoice_summary);
        noCorrect = 0;
        noAnswered = 0;

        sub.setOnClickListener(this);
        initialiseQuestions();

        generateSummary();

        generateQuestion(0);
    }

    private void getCurrentScore() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0
        TextView historyCurrentScore = (TextView) findViewById(R.id.current_highScore);

        // getting String
        int historyScore = pref.getInt("historyScore", _null);

        if (historyScore >= 1) {
            // Display name on textView
            historyCurrentScore.setText(String.format("Your current score is %s.", historyScore));
        }
    }

    private void generateQuestion(int noQ) {

        opts.removeAllViews();
        currentQuestion = questions.get(noQ);
        question.setText(currentQuestion.getQuestion());

        int i = 1;
        for (String ans : currentQuestion.getAnswers()) {
            RadioButton rb = new RadioButton(this);
            rb.setText(ans);
            rb.setId(i++);
            opts.addView(rb);
        }
    }

    private void initialiseQuestions() {

        questions = new Vector<MultiChoiceQuestion>();
        MultiChoiceQuestion q1 = new MultiChoiceQuestion("Who captained South Africa during their 1999 World Cup cricket campaign?");
        q1.addAnswer("Hansie Cronjé", true);
        q1.addAnswer("Graeme Smith", false);
        q1.addAnswer("Shaun Pollock", false);
        q1.addAnswer("Lance Klusener", false);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Who conquered England in 1066?");
        q1.addAnswer("Harold Godwinson", false);
        q1.addAnswer("Edward the Confessor", false);
        q1.addAnswer("William the Conqueror", true);
        q1.addAnswer("Alfred the Great", false);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Who first climbed Mount Everest?");
        q1.addAnswer("Edward Whymper", false);
        q1.addAnswer("Edmund Hillary", true);
        q1.addAnswer("Chris Bonnington", false);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Who first reached the South Pole?");
        q1.addAnswer("Captain Scott", false);
        q1.addAnswer("Roald Amundsen", true);
        q1.addAnswer("Edmund Hillary", false);
        q1.addAnswer("Robert Peary", false);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Following the abandonment of apartheid, in what year were South Africa re-admitted to the Commonwealth?");
        q1.addAnswer("1992", false);
        q1.addAnswer("1997", false);
        q1.addAnswer("1998", false);
        q1.addAnswer("1994", true);

        questions.add(q1);
        q1 = new MultiChoiceQuestion("Which South African company sells about 80% of the world's rough gem diamonds through its London office?");
        q1.addAnswer("De Beers", true);
        q1.addAnswer("Gold Fields", false);
        q1.addAnswer("Becker", false);
        q1.addAnswer("AngloGold", false);

    }

    private void generateSummary() {
        summary.setText(noCorrect + " answers out of " + noAnswered
                + " attempted of " + noQuestions + " questions");
    }

    public void onClick(View arg0) {
        noAnswered++;

        int id;
        id = opts.getCheckedRadioButtonId();

        if (currentQuestion.isCorrect(id))
            noCorrect++;

        generateSummary();

        if (noAnswered == noQuestions) {
            finish();
            Intent intentBundle = getScore();
            startActivity(intentBundle);
        }
        else
            generateQuestion(noAnswered);
    }

    private Intent getScore() {
        // Create an intent pass class name
        Intent intentBundle = new Intent(History_Questions_Activity.this, Select_Game_Activity.class);

        Bundle bundle = new Bundle();

        bundle.putInt("historyScore", noCorrect);

        intentBundle.putExtras(bundle);
        return intentBundle;
    }
}